#ifndef DBCONTROLLER_H
#define DBCONTROLLER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QAbstractTableModel>
#include "cryptocontroller.h"

class FriendsObject2 { // Объект как и в authcontroller
    public:
        FriendsObject2 (const QString &FriendId,
                       const QString &FriendName,
                       const QString &Photo,
                       const QString &Status);

        QString FriendId() const;
        QString FriendName() const;
        QString Photo() const;
        QString Status() const;
    private:
        QString ob_friendid;
        QString ob_friendname;
        QString ob_photo;
        QString ob_status;
};


class FriendsModel2 : public QAbstractListModel { // модель как и в authcontroller
    Q_OBJECT
public:
    enum DataRoles {
        FriendIdRole,
        FriendNameRole,
        PhotoRole,
        StatusRole
    };

    FriendsModel2(QObject *parent = 0);

    void addFriend(const FriendsObject2 & newFriend);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    Q_INVOKABLE void updateModel();
    Q_INVOKABLE void clearModel();
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FriendsObject2> ob_friends;

};


class dbcontroller : public QObject // класс для работы с базами данных
{
    Q_OBJECT
public:
    Cryptocontroller cryptocontroller; // добавляем сюда возможность шифровать базу
    FriendsModel2 dbModel; // модель для хранения друзей
    explicit dbcontroller(QObject *parent = nullptr);
    QSqlDatabase db; // переменную для вызова операций sql
    QString fileName;  // переменная для хранения имени создаваемого файла
    void initTable(QString filename, QString friends); // функция для создания новой базы
    void downModel(QString filename); // функция для отображения существующей базы
    ~dbcontroller(); // деструктор класса
signals:

public slots:
    void add_friend(QString id, QString name, QString status); // функция для добавления человека (id, имя, cтатус) в базу
    void delete_friend(QString friend_id); // функция удаления друга из базы
};

#endif // DBCONTROLLER_H
